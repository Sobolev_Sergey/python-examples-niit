import unittest

import task61

class TestTo_roman(unittest.TestCase):

    def setUp(self):
        print('start')

    def test_normal_to_roman(self):
        self.assertEqual(task61.to_roman(1),'I')
        self.assertEqual(task61.to_roman(4321),'MMMMCCCXXI')
        self.assertEqual(task61.to_roman(4567),'MMMMDLXVII')

    def test_NonValidInput(self):
        self.assertRaises(task61.NonValidInput, task61.to_roman, -1234)       # n < 1
        self.assertRaises(task61.NonValidInput, task61.to_roman, 0)           # n < 1
        self.assertRaises(task61.NonValidInput, task61.to_roman, 5001)        # n > 5000
        self.assertRaises(task61.NonValidInput, task61.to_roman, 'string')    # n - string
        self.assertRaises(task61.NonValidInput, task61.to_roman, '123')       # n - string
        self.assertRaises(task61.NonValidInput, task61.to_roman, 132.4)       # n - float

    def tearDown(self):
        print('end')

if __name__ == '__main__':
    unittest.main()
