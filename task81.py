'''
Напишите функцию odd_primes(end, start), которая ищет все простые числа в диапазоне от заданного числа
start (по умолчанию 3) до заданного числа end.
Запустите ее:
- Три раза последовательно в диапазоне от 3 до 10000, от 10001 до 20000, от 20001 до 30000.
- Также запустите ее три раза с теми же аргументами, но каждую в отдельной потоке с помощью  threading.Thread.
Не забудьте стартануть треды и дождаться их окончания.
- Также запустите ее три раза с теми же аргументами, но каждую в отдельной потоке с помощью  multiprocessing.Process.
Не забудьте стартануть процессы и дождаться их окончания.

Замерьте время исполнения каждого варианта. Подумайте над результатами.
И не запускайти эти скрипты в PyCharm в Debug режиме, это все замедлит.
'''
import math
import threading
from time import time
import multiprocessing


def odd_primes(end, start):                         # Простые числа в реализации по алгоритму Решето Эратосфена
    a = [True] * end                                # Все целые числа от 2 до end (2, 3, 4, …, n).
    for i in range(2, int(math.sqrt(end+1))):       # Берëм невычеркнутые числа от 2 до sqrt(n)
        for j in range(i * 2, end, i):              # Вычëркиваем числа, кратные невычеркнутому
            a[j] = False
    b = [i for i in range(start, end) if a[i]]      # Все невычеркнутые числа - простые
    return b

if __name__ == '__main__':      # у multiprocessing есть ограничение по организации файлов проекта
                                # поэтому для Windows запуск оборачиваем в if __name__ == '__main__':
                                # Инструкция if __name__=='__main__': разрешает запускать параллельные
                                # процессы только из основного. А вот дочерний процесс уже не сможет
                                # породить ещё один аналогичный процесс.


    my_range = [(10000, 3), (20000, 10001), (30000, 20001)]

    # 1. Последовательный запуск:
    start = time()
    for i in range(3):
        odd_primes(my_range[i][0], my_range[i][1])
    print('Serial launch: {}'.format(float(time() - start)))

    # 2. Запуск в отдельном потоке с помощью  threading.Thread
    start = time()
    threads = []
    for i in range(3):
        thr = threading.Thread(target=odd_primes, args=(my_range[i]))
        thr.start()
        threads.append(thr)

    for thr in threads:
        thr.join()
    print('Run in a separate thread: {}'.format(float(time() - start)))

    # 3. Запуск в отдельной потоке с помощью  multiprocessing.Process
    start = time()
    process = []
    for i in range(3):
        p = multiprocessing.Process(target=odd_primes, args=(my_range[i]))
        p.start()
        process.append(p)

    for p in process:
        p.join()
    print('Run in a separate process multiprocessing: {}'.format(float(time() - start)))

'''
В диапазоне (10000, 3), (20000, 10001), (30000, 20001)
время выполнения примерно одинаково у всех трех вариантов запуска. 
Менялось рандомно в диапозоне от 0.045... до 0.063...

В диапазоне (1000000, 3), (2000000, 1000001), (3000000, 2000001)
Результат всегда получается примерно:
Serial launch: 10.650298595428467
Run in a separate thread: 9.830484867095947
Run in a separate process multiprocessing: 6.671248197555542

Вывод: использование multiprocessing при вычислении больших данных является более эффективным
подробности: https://habrahabr.ru/post/149420/
'''

