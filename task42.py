'''
Напишите класс WrapStrToFIle, который будет иметь одно вычисляемое свойство (property) под названием content.
В конструкторе класс должен инициализовать атрибут filepath, путем присваивания результата функции mktemp
библиотеки tempfile. При попытке чтения свойства content должен внутри кода свойства открываться файл,
используя атрибут filepath (с помощью функции open,
из этого файла читается все содержимое и возвращается из свойства.
Если файл не существует, то возникает ошибка, поэтому должна быть обертка вокруг открытия
файла на чтение (try...except),  с помощью которого будет возвращаться 'Файл еще не существует'.
При присваивании значения свойству content файл по указанному пути должен открываться
на запись и записываться содержимое. Не забудьте закрывать файл после чтения или записи.
При удалении атрибута content, должен удаляться и файл. Продолжение на слайде ниже.
'''
import os
import tempfile

class WrapStrToFile:
    def __init__(self):
        # здесь инициализируется атрибут filepath, он содержит путь до файла-хранилища
        self.filepath = tempfile.mktemp()

    @property
    def content(self):
        # попытка чтения из файла, в случае успеха возвращаем содержимое
        # в случае неудачи возращаем 'Файл еще не существует'
        file = open(self.filepath)
        try:
            my_string = file.read()
            #file.close()
            return my_string
        except FileNotFoundError:
            return 'Файл еще не существует'
        finally:
            file.close()

    @content.setter
    def content(self, value):
        # попытка записи в файл указанное содержимого
        file = open(self.filepath, 'w')
        file.write(value)
        file.close()

    @content.deleter
    def content(self):
        if os.path.exists(self.filepath):
        # удаляем файл, например, через ф-ию unlink либы os
            os.unlink(self.filepath)

wstf = WrapStrToFile()
print(wstf.content)
# Файл еще не существует
wstf.content = 'test str'
print(wstf.content)
# test str
wstf.content = 'text 2'
print(wstf.content)
# text 2
del wstf.content # после этого файла не существует
