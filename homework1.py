from random import randint

# Функция fio, которая принимает одну строку, состоящую из ФИО (например, "Ladoshkin Nikita Evgenievich")
#  и возвращает словарь с ключами name, surname, patronymic
# (пример, {'name': 'Nikita', 'surname': 'Ladoshkin', 'patronymic': 'Evgenievich'})
def fio(str):
    arr = str.split(' ')
    dictionary = {'name': arr[1], 'surname': arr[0], 'patronymic': arr[2]}
    return dictionary

print(fio('Sobolev Sergey Konstantinovich'))


# Функция sort_str, которая принимает массив строк (['aaa', 'c', 'qq'])
# и возвращает массив этих строк, отсортированных по длине (['c', 'qq', 'aaa'])
def sort_str(strings):
    return sorted(strings, key=lambda x: len(x))

print(sort_str(['aaa', 'c', 'qq']))


# Упрощенная собственная реализация simple_range, которая примимает два числа (1, 5),
# а возвращает массив от первого числа включительно до второго невключительно ([1, 2, 3, 4])
def simple_range(a, b):
    arr = []
    while a != b:
        arr.append(a)
        a += 1
    return arr

print(simple_range(1, 5))


# Функция use_rand без параметров, которая получает результат ф-ии randint библиотеки random
# c аргументами 1 и 50, до тех пор, пока не получите число 42, затем вернете этот результат
def use_rand():
    a = 0
    while (a != 42):
        a = randint(1, 50)
    return a

print(use_rand())