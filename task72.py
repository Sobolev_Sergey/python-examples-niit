'''
Парсинг (разбор) html страничек очень часто предлагают в качестве тестового задания для оценки способностей.
Напишите класс, который запрашивает содержимое веб-странички с помощью стандартной библиотеки urllib.request
и вытаскивает у нее заголовок (содержимое тэга title) любым придуманным вами способом (с помощью стандартной
или скаченной библиотеки, как угодно). Например, если вы скачаете страничке по адресу "http://python.org/",
то можете найти у нее указанный тег и текст внутри него - "<title>Welcome to Python.org</title>"
'''

import urllib.request
import re

class MyParser:
    def __init__(self, url):
        site = urllib.request.urlopen(url)
        print(str('\n'.join(re.findall('<title>(.*)</title>', str(site.read())))))

parser = MyParser('http://python.org/')