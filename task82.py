'''
Создайте класс, один из методов которого способен принимать не одиночный url,
а массив url'ов и возвращать массив строк, полученных  из заголовков html страницек,
которые вы получите из этих заданных url'ов.
Используйте ThreadPoolExecutor из библиотеки concurrent.futures.
Его максимальное количество воркеров должно быть равно количеству ядер на компьтере.
Получение каждого отдельного урла должно запускаться с помощью метода submit у ThreadPoolExecutor.
Получение результата от отдельного урла должно происходить через объект Future,
который вернет метод submit. Подробности в документации.
Проверьте работу вашего класса с помощью любых 20 url'ов.
'''

import urllib.request
from concurrent.futures import ThreadPoolExecutor, as_completed
import bs4
import psutil


class MyParser:
    def __init__(self, url):
        self.url = url

    def get_title(url):
        with urllib.request.urlopen(url) as next_url:
            url_content = next_url.read()
        soup = bs4.BeautifulSoup(url_content, "html.parser")
        return soup.title.string

    def get_title_submit(self):
        temp = []
        with ThreadPoolExecutor(max_workers=psutil.cpu_count()) as pool:        #multiprocessing.cpu_count
            results = [pool.submit(MyParser.get_title, i) for i in self.url]
            for future in as_completed(results):
                temp.append(future.result())
            return temp

urls = [
    'https://vk.com/',
    'https://ok.ru/',
    'https://mail.ru/',
    'https://www.avito.ru/',
    'https://google.com/',
    'https://www.gismeteo.ru/',
    'https://russian.rt.com/',
    'https://lady.mail.ru/',
    'http://www.avtoru52.ru/',
    'https://otvet.mail.ru/',
    'https://sinoptik.com.ru/',
    'https://my.mail.ru/',
    'https://ria.ru/',
    'https://www.nnov.kp.ru/',
    'https://news.rambler.ru/',
    'https://lenta.ru/',
    'https://www.gazeta.ru/',
    'https://www.olx.ua/',
    'https://rutube.ru/',
    'https://life.ru/'
]

parser = MyParser(urls)
for i in parser.get_title_submit(): print(i)
