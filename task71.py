'''
Напишите программу, которая уничтожает файлы и папки по истечении заданного времени.
Вы указываете при запуске программы путь до директории, за которой нашему скрипту необходимо следить.
После запуска программа не должна прекращать работать, пока вы не остановите ее работу с помощью Ctrl+C
(подсказка: для постоянной работы программы необходим вечный цикл, например, "while True:",
при нажатии Ctrl+C автоматически остановится любая программа).
Программа следит за объектами внутри указанной при запуске папки и удаляет их тогда,
когда время их существования становится больше одной минуты для файлов и больше двух минуты для папок
(то есть дата создания отличается от текущего момента времени больше чем на одну/две минуту).
Ваш скрипт должен смотреть вглубь указанной папки.
Например, если пользователь создаст внутри нее папку, внутри нее еще одну, а внутри этой какой-то файл,
то этот файл должен удалиться первым (так как файлу положено жить только одну минуту, а папкам две).
Вам понадобятся библиотеки os и shutil. Внимательно перечитайте задание и учтите возможные ошибки.
'''
import argparse
import os
import shutil
from time import time

parser = argparse.ArgumentParser()
parser.add_argument('directory', help='The path to the directory where our script must to watch', type=str)
path = parser.parse_args().directory


def MyWatcher(path):
    for d, dirs, files in os.walk(path):
        for f in files:
            if time() - os.path.getctime(os.path.join(d, f)) > 60:
                os.remove(os.path.join(d, f))
        for dr in dirs:
            if time() - os.path.getctime(os.path.join(d, dr)) > 120:
                shutil.rmtree(os.path.join(d, dr))


if os.path.isdir(path):
    while True:
        MyWatcher(path)
else:
    print('This path does not exist')
    exit(1)
