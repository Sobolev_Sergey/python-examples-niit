'''
Напишите программу, которая выводит вместо чисел, кратных трем слово «Fizz»,
а вместо чисел, кратных пяти — слово «Buzz».
Если число кратно и 3, и 5, то программа должна выводить слово «FizzBuzz»
'''

def fizzbuzz(args):
    arr = []
    for i in args:
        temp = ''
        if i % 3 == 0:
            temp = 'Fizz'
        if i % 5 == 0:
            temp = 'Buzz'
        if i % 15 == 0:
            temp = 'FizzBuzz'
        #arr.append(temp != '' and temp or i)
        arr.append(temp or i)
    return arr

print(fizzbuzz(range(1, 21)))