'''
Напишите функцию to_roman, которая принимает целое число, а возвращает строку,
отображающую это число римскими цифрами. Например, на вход подается 6, вернет "VI".
Например, на вход подается 23, вернет "XXIII". Входные данные должны быть в диапазоне
от 1 до 5000, если подается число не в этом диапазоне или не число, то должны
выбрасываться ошибка типа NonValidInput. Этот тип ошибки вы должны создать сами.
Также необходимо в папке с файлом, содержащей вашу функцию, создать файл tests.py,
внутри которой необходимо определить тесты для вашей функции.
Тесты должны покрывать все возможные поведения функции,
включая порождения ошибки при плохих входных данных.
'''

class NonValidInput(Exception):
    pass

def to_roman(n):
    result = ''
    if isinstance(n, int) and 0 < n <= 5000:
        for arabic, roman in zip((1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1),
                                 'M     CM   D    CD   C    XC  L   XL  X   IX V  IV I'.split()):
            result += n // arabic * roman
            n %= arabic
            print('({}) {} => {}'.format(roman, n, result))
        return result
    else:
        raise NonValidInput()
to_roman(4567)
'''
to_roman(4321)

#print('({}) {} => {}'.format(roman, n, result))

#Функция zip объединяет в кортежи элементы из последовательностей переданных в качестве аргументов.

result += n // arabic * roman
result() = result() + n(4321) // arabic(1000)   +=    4 * M = MMMM

n %= arabic
n(4321) = n(4321) % arabic(1000) = 321

(M) 321 => MMMM
(CM) 321 => MMMM
(D) 321 => MMMM
(CD) 321 => MMMM

result += n // arabic * roman
result(MMMM) = result(MMMM) + n(321) // arabic(100)  +=  3 * C = MMMMCCC

n %= arabic
n(321) = n(321) % arabic(100) = 21
и т.д. до конца

(C) 21 => MMMMCCC
(XC) 21 => MMMMCCC
(L) 21 => MMMMCCC
(XL) 21 => MMMMCCC
(X) 1 => MMMMCCCXX
(IX) 1 => MMMMCCCXX
(V) 1 => MMMMCCCXX
(IV) 1 => MMMMCCCXX
(I) 0 => MMMMCCCXXI
'''