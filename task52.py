'''
Во втором задании вам необходимо написать консольную утилиту, используя argparse.
Она должна принимать позиционный аргумент dirpath - это будет директория, внутри
которой утилита создаст новую папку. Название новой папки будет зависеть от указанных опций.
Если указана опция -y, то в назнании присуствует текущий год.
Если -m, то номер текущего месяца.
Если -d, то номер текущего дня.
Опции можно комбинировать, подробности на примере.
Если папка с заданным названием уже существует, то нужно просто выводить предупреждение
об этом и больше ничего не делать. Если не указано ни одной опции, то утилита должна
создать папку с именем 'unknown'. Чтобы узнать текущий месяц, день или год,
нужно прочитать атрибуты объекта, который появится в результате вызова функции
datetime.now() библиотеки datetime. Способ создания папки Вам придется найти самим.
$ python createdatedir.py -y -m -d /tmp // эта комадна создаст
                                        // директорию '2017-7-13' в директории /tmp
$ python createdatedir.py -y -m -d /tmp // повторная команда выдаст сообщение
                                        // о существовании такой папки
$ python createdatedir.py -d /tmp       // эта комадна создаст директорию '13'
                                        // в директории /tmp
$ python createdatedir.py /home/user    // эта комадна создаст директорию 'unknown'
                                        //в директории /home/user
'''
import argparse
from datetime import datetime
import os
import itertools

parser = argparse.ArgumentParser()
parser.add_argument('-y', '--y', action="store_true", help='If you specify the -y option,\
                                                            then the name contains the current year.')
parser.add_argument('-m', '--m', action="store_true", help='If -m is the number of the current month.')
parser.add_argument('-d', '--d', action="store_true", help='If -d, then the number of the current day.')
parser.add_argument('dirpath', help='Directory inside which utility will create a new folder', type=str)
args = parser.parse_args()

year = str(datetime.now().year)
month = str(datetime.now().month)
day = str(datetime.now().day)

name_folder = "-".join(list(itertools.compress([year, month, day], [args.y, args.m, args.d])))

if name_folder == "":
    name_folder = 'unknown'


# функции для создания папки по указанному пути с указанным именем
def create_folder(dirpath, name_folder):
    os.chdir(dirpath)
    if os.path.exists('{}\\{}'.format(dirpath, name_folder)):
        print('Folder with the specified name already exists!')
    else:
        os.mkdir(str(name_folder))
        print('The folder is created: {}{}\\'.format(dirpath, name_folder))

        '''
        os.mkdir(path, mode=0o777, *, dir_fd=None) - создаёт директорию. OSError, если директория существует.
        Для проверки что такая папка уже существует, можно использовать try-exept-else
        Но на ru.stackoverflow.com пищут что конструкция try-exept-else многими считается плохо читаемой. 
        Каждый раз, когда файла нет, вызываеся прерывание ОС (на нем строится механизм обработки исключения)
        самостоятельно, это раcточительно.
        Поэтому использовал os.path.exists(path_to_file)
        А как правильно?
        '''


create_folder(args.dirpath, name_folder)
