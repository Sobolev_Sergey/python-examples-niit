from random import randint
from time import time

#Создать функцию unic_abs, которая будет принимать любое количество чисел,
# а вернет массив их модулей без повторений (args, abs, set), записать определение в файл
def unic_abs( *args):
    array = []
    for arg in args:
        array.append(abs(arg))
    return set(array)

print (unic_abs(2, 3, -1, 0, 5, 4, 2, 3))

#Создать массив от -10 до 10, прокинуть в unic_abs, вывести
arr10 = [-10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print(format(unic_abs(*arr10)))

#Создать ее с помощью range, прокинуть в unic_abs, вывести
print(unic_abs(*range(-10,10)))

#Использовать библиотеку random c методом randint, создать массив из 100 чисел (разброс от -100 до 100),
# прокинуть в unic_abs, вывести
arr100 = []
for _ in range (100):
    arr100.append(randint(-100, 100))
print(len(arr100))
print(format(unic_abs(*arr100)))

#Создать функцию create_randlist, которая будет создавать этот массив одной строкой
# (lambda, list comprehension, range), записать определение в файл
create_randlist = lambda: [randint(-100, 100) for _ in range(100)]
print(format(unic_abs(*create_randlist())))

#Создать функцию check_abs, которая будет проверять циклом каждый элемент массива на положительность
def check_abs(x):
    for i in x:
        if i > 0:
            return True
    return False

print(format(check_abs(arr100)))

#Создать эту функцию в одну строчку (lambda, all)
check_abs_lambda = lambda x: all([True for i in x if i < 0])

print(format(check_abs_lambda(arr100)))

#Написать декоратор timeit, которые с помощью библиотеки time, метода time выдает время исполнения
def timeit(fun):
    def wrapper():
        t1 = time()
        fun()
        t2 = time()
        print(format(t2 - t1))
    return wrapper

#создать функцию run_all, которая массив от create_randlist закинет в unic_abs,
# а результат в check_abs, обернуть это timeit, записать в файл, запустить через консоль
@timeit
def run_all():
    print(check_abs(unic_abs(*create_randlist())))

run_all()